﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MADIVE.Models
{
    public class CheckoutInfoModels
    {
        [Required]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required]
        [Display(Name = "Shipping Address")]
        public string ShippingAddress { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})", ErrorMessage = "Phone Number is not valid.")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Card Name")]
        [RegularExpression(@"^[A-Za-z\s]*$", ErrorMessage = "Card Name is not valid.")]
        public string CardName { get; set; }

        [Required]
        [Display(Name = "Card Number")]
        [CreditCard]
        public string CardNumber { get; set; }

        [Required]
        [Display(Name = "Month")]
        [RegularExpression(@"^0[1-9]|1[012]$", ErrorMessage = "Month input is not valid.")]
        public string ExpirationMonth { get; set; }

        [Required]
        [Display(Name = "Year")]
        [RegularExpression(@"^[0-9]{4}$", ErrorMessage = "Year input is not valid.")]
        public string ExpirationYear { get; set; }

        [Required]
        [Display(Name = "CCV")]
        [RegularExpression(@"^[0-9]{3,4}$", ErrorMessage = "CVV code is not valid.")]
        public string CCV { get; set; }
    }
}