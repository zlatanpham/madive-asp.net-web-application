﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MADIVE.Models
{
    public class ProductWithCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { get; set; }
        public string CategoryName { get; set; }
        public List<Product> Products { get; set; }
    }
}