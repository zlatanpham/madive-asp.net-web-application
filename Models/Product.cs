﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MADIVE.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "Invalid value for price")]
        public double Price { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage="Invalid value for quantity")]
        public int Quantity { get; set; }
    }
}