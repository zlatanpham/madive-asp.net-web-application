﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MADIVE
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "product_details",
                "product/{id}",
                new { controller = "Product", action = "Index" }
            );

            routes.MapRoute(
                "category_list",
                "category/{name}",
                new { controller = "Category", action = "Index", page = 1 }
            );

            routes.MapRoute(
                "category_page",
                "category/{name}/page/{page}",
                new { controller = "Category", action = "Index"}
            );

            routes.MapRoute(
                name: "Paging",
                url: "page/{page}",
                defaults: new { controller = "Home", action = "Index"}
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
