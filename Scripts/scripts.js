(function($){	

/* Wait until all DOM elements are rendered
 */
$(window).ready(function(){	

    /* 
     * Show and hide the menu sidebar when user click on
     * the menu icon on the top right of the webpage
     */
    var sidebar = {
        container: $(".sidebar-container"),
        menuButton: $(".sidebar-button"),
        loginButton: $(".show-login-box"),
        hideMenuButton: $(".hide-menu"),
        categoryContainer: $(".category-list-wrap"),
        showCategoryButton: $(".show-category"),
        backToMenuButton: $(".back-to-menu"),
		showHideSidebar : function(e){
			// Prevent default click behavior
			e.preventDefault();
			
			if($(this).hasClass("shown"))
			{
				$(this).removeClass("shown");
				sidebar.container.removeClass("shown");
			}
			else
			{
				$(this).addClass("shown");
				sidebar.container.addClass("shown");
			}	
		},
		showLogInBox : function(e)
		{
		    e.preventDefault();
		    login.showLoginForm(e);
		},
		hideMenu: function(e)
		{
		    e.preventDefault();
		    sidebar.container.removeClass("shown");
		    sidebar.menuButton.removeClass("shown");
		},
		showCategory: function(e)
		{
		    e.preventDefault();
            sidebar.categoryContainer.addClass("shown");
		},
		hideCategory: function(e)
		{
		    e.preventDefault();
		    sidebar.categoryContainer.removeClass("shown");
		}
    };

    sidebar.loginButton.click(sidebar.showLogInBox);
    sidebar.menuButton.click(sidebar.showHideSidebar);
    sidebar.hideMenuButton.click(sidebar.hideMenu);
    sidebar.showCategoryButton.click(sidebar.showCategory);
    sidebar.backToMenuButton.click(sidebar.hideCategory);

    $(".overlay").click(sidebar.hideMenu);


	var login = {
	    form: $(".login-form"),
	    closeButton: $(".login-close"),

	    closeLoginForm: function (e) {
	        e.preventDefault();
	        login.form.removeClass("shown");
	    },
	    showLoginForm: function (e) {
	        e.preventDefault();
	        sidebar.hideMenu(e);
	        login.form.addClass("shown");
	    }
	};

	login.closeButton.click(login.closeLoginForm);


    /*
     * Resize the popup dialog of product container which is called
     * by ajax function. This dialog will be resized to fit the border
     * area of the webpage
     */
	$(window).resize(function(){
		$(".product-detail").css({
			"width" : $(window).width() - 80,
			"height" : $(window).height() - 80
		});

		if (!$(".product-page").size())
		{
		    $(".detail-right-wrap").css({
		        "margin-top": parseInt($(".product-detail").height() - $(".detail-right-wrap").height() - 40) / 2
		    });
		}
	});
	
	$(window).resize();
	

	var cart = {
	    removeButton: $(".remove-item"),
	    changeQuantityButton: $(".minus, .plus"),
        updateQuantityButton : $(".update-quantity"),
	    form: $("form#updatecart"),
	    id: $("form#updatecart input[name='id']"),
	    quantity: $("form#updatecart input[name='quantity']"),        

	    previewTitle: $(".preview-last-items"),
	    previewContainer: $(".preview-items"),
	    previewPrice: $(".preview-total-price"),
	    previewSize: $(".num-of-item"),

	    /*
         * Update the information inside quick preview cart when
         * users update an item through AJAX
         */
	    updatePreviewCart : function(data)
	    {
	        var $numofitem = data.size;
	        var $price = data.total;
	        var $items = data.items;
	        var $url_pattern = cart.previewContainer.attr("data-refer-link");

	        cart.previewTitle.text($numofitem <= 5 ? $numofitem + " items in cart" : "Last 4 of " + $numofitem + " items");
	        cart.previewPrice.text("$" + $price);
	        cart.previewSize.text($numofitem);

            // Remove all old DOM element inside the container
	        cart.previewContainer.html("");
	        // Render the items into unorder list
	        for(var i = 0; i < $items.length; i++)
	        {
	            var $item = $items[i];
	            var $url = $url_pattern + "/" + $item.Id;
	            var $link = '<a href="' + $url + '">';
	            $link += '<span class="preview-image" style="background-image:url(' + $item.Image + ')"></span>';
	            $link += '<h6>' + $item.Name + '</h6>';
	            $link += '<p>Price: $' + $item.Price + '</p>';
	            $link += '<p>Quantity: ' + $item.Quantity + '</p>'
	            $link += '</a>';
	            var $li = $("<li></li>").html($link);
	            cart.previewContainer.append($li);
	        }
	    },

	    /*
         * Remove item from cart by assigning values of item id and set quantity to 0
         * to the inputs tag inside a from then trigger to submit the data
         */
	    removeItem : function(e)
	    {
	        e.preventDefault();
	        cart.id.val($(this).attr("data-product-id"));
	        cart.quantity.val(0);
	        cart.form.submit();
	    },

	    /*
         * Plus or minus the value inside an input field which carry
         * the quantity data of a product
         */
	    changeQuantity : function(e)
	    {
	        e.preventDefault();
	        $quantity = $(this).hasClass("minus") ? $(this).next() : $(this).prev();
	        $current = parseInt($quantity.val());

	        var $button = $(this).parent().next();
	        
	        if($(this).hasClass("minus") && $current > 0)
	        {
	            $quantity.val($current - 1);
	        }
	        else if($(this).hasClass("plus"))
	        {
	            $quantity.val($current + 1);
	        }

	        var $existing = parseInt($quantity.attr("data-existing-value"));
	        var $change = parseInt($quantity.val());

	        if ($existing  != $change) {
	            $button.removeClass("blur");
	        }
	        else {
	            $button.addClass("blur");
	        }
	    },

	    /*
         * Update the quantity of an item from cart by assigning values of item id and quantity
         * to the inputs tag inside a from then trigger to submit the data
         */
	    updateQuantity : function(e)
	    {
	        e.preventDefault();
	        var $input = $(this).prev().find("input[name='quantity']");
	        var $existing = parseInt($input.attr("data-existing-value"));
	        var $current = parseInt($input.val());

	        //In the case there is no change in quantity of product
            //then, prevent user to update the data
	        if($existing == $current)
	        {
	            return;
	        }

	        cart.id.val($(this).attr("data-product-id"));
	        cart.quantity.val($input.val());
	        cart.form.submit();
	    }
	}

	cart.removeButton.click(cart.removeItem);
	cart.updateQuantityButton.click(cart.updateQuantity);
	cart.changeQuantityButton.click(cart.changeQuantity);


	var product = {
        ajax : $(".ajax-container"),
		container : $(".product-detail"),
		closebutton : ".product-close",
		quantitybutton: ".minus-quantity,.plus-quantity",
		quantity: $("input[name='quantity']"),
        preview: $(".product-image-preview"),
        token: $("input[name='__RequestVerificationToken']"),

		title: $(".product-title,.page-list a:last-child"),
		price: $(".product-price span"),
		desc: $(".product-desc"),
        detail: $(".product-details"),
        category: $(".product-category span, .page-list a:nth-child(2)"),
        addToCartButton: ".add-to-cart",

		
        item: ".grid > li > a",

	    /*
         * Add a product to card using AJAX
         * Update the cart preview when success
         */
        addToCart : function(e)
        {
            // Prevent default click behavior
            e.preventDefault();
            var $button = $(this);
            var $link = $button.attr("href");
            var $id = $button.attr("data-product-id");
            var $quantity = parseInt($("input[name='quantity']").val());
            var $token = $("input[name='__RequestVerificationToken']").val();
            var $existing_quantity = parseInt($("input[name='quantity']").attr("data-exist-value"));

            if ($quantity == $existing_quantity)
            {
                return;
            }

            $button.text($button.attr("data-product-wait"));

            var options = {
                url: $link,
                type: "POST",
                data: {
                    __RequestVerificationToken : $token,
                    id : $id,
                    quantity : $quantity
                },
                dataType: "json"
            }

            $.ajax(options).done(function (data) {
                var done = setTimeout(function () {
                    if ($quantity > 0)
                    {
                        $button.text($button.attr("data-product-update"));
                    }
                    else
                    {
                        $button.text($button.attr("data-product-add"))
                    }
                    $("input[name='quantity']").attr("data-exist-value", data.quantity);
                    // Update the preview cart
                    cart.updatePreviewCart(data);
                }, 500);
                
            });

        },

	    /*
         * Using AJAX to preload detailed of a product
         * from grid view
         */
		showDialog : function(e)
		{
			//Prevent default click behavior
		    e.preventDefault();
		    var $link = $(this).attr("href");

		    // Showing loading effect for waiting response from server
		    product.container.addClass("loading");

		    product.ajax.load($link + " .product-container", function () {
		        product.container.html($(".product-container").html());
                // Resize to organize elements in a neat layout
		        $(window).resize();
		        setTimeout(function () {
		            product.container.removeClass("loading").addClass("shown");
		        }, 500);

		    });
		},
		
		closeDialog : function(e)
		{
			//Prevent default click behavior
			e.preventDefault();
			product.container.removeClass("shown");
		},	
		
		changeQuantity : function(e)
		{
		    e.preventDefault();
		    var $quantity = $("input[name='quantity']");
		    var $current = parseInt($quantity.val());
		    	
			if($(this).hasClass("minus-quantity") && $current > 0)
			{
                $current--;
                $quantity.val($current);
			}
            else if($(this).hasClass("plus-quantity") && $current < 1000)
			{
			    $current++;
			    $quantity.val($current);
			}
		},
		
	};
	
	$(document).on("click", product.closebutton, product.closeDialog);
	$(document).on("click", product.item, product.showDialog);
	$(document).on("click", product.quantitybutton, product.changeQuantity);
	$(document).on("click", product.addToCartButton, product.addToCart);


    /*
     * Infinite scrolling effect for loading more products
     * This implementation renders new items by capturing 
     * items from the next page and stopping after reaching 
     * the lastest page
     */
	var $grid = $(".grid");
	var $container = $(".ajax-items-container");
	var $preload = $(".preload");
	var $page = parseInt($grid.attr("data-page"));          // Current page
	var $max_page = parseInt($grid.attr("data-max"));       // Max number of pages can be reached
	var $next_link = $grid.attr("data-next-link");          // The link of current page

	var $onloading = false;                                 // Keep the state of loading progess

	$(window).scroll(function () {
        
	    var $infinite = $(document).height() - $(window).height();      // Keep the distance of scrolling bar to bottom of page

        // Only run the inside implementation if 
	    // the previous process is completed 
	    // Users scroll to the bottom of the page
        // The last page is not reached
	    if(!$onloading && $(window).scrollTop()  == $infinite && $page <= $max_page)
	    {
	        $onloading = true;
	        $preload.addClass("shown"); // Show the loading icon
	        $container.load($next_link + " .item-box", function () {

	            $page++;
	            $next_link = $next_link.replace(/\/page\/[0-9]*/, '/page/' + $page);	// Replace link with new page
	            $new_items = $($container.html());
	            $grid.append($new_items);
                
	            $timer = setTimeout(function () {
	                $onloading = false;
	                $preload.removeClass("shown");
	            }, 300);
	            
	        });
	    }
	    

	})


});
	
})(jQuery);

