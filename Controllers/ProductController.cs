﻿using MADIVE.ADL;
using MADIVE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MADIVE.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        MadiveContext _db = new MadiveContext();

        public ActionResult Index(int Id)
        {
            var pqut = 0;

             var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>)Session["cart"];

            var product_in_cart = cart.Find(p => p.Id == Id);

            if(product_in_cart != null)
            {
                pqut = product_in_cart.Quantity;
            }

            // Get the item detail along with its three 
            // same-category products
            var model = _db.Products
                .Where(p => p.Id == Id)
                .Join(
                    _db.Categories,
                    p => p.CategoryId,
                    c => c.Id,
                    (p, c) => new ProductWithCategory 
                    {
                        Name = p.Name,
                        Price = p.Price,
                        Description = p.Description,
                        Detail = p.Detail,
                        Image = p.Image,
                        Id = p.Id,
                        Quantity = pqut,
                        CategoryId = c.Id,
                        CategoryName = c.Name,
                        Products = _db.Products
                            .Where( p2=> p2.Id != Id && p2.CategoryId == p.CategoryId)
                            .OrderBy(x=> Guid.NewGuid())
                            .Take(3).ToList()
                    })
                .Take(1).First();
            
            
            return View((ProductWithCategory)model);
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}