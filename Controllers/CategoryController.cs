﻿using MADIVE.ADL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using MADIVE.Models;

namespace MADIVE.Controllers
{
    public class CategoryController : Controller
    {
        MadiveContext _db = new MadiveContext();

        // GET: Category
        // /Category/
        public ActionResult Index(string name, int page = 1)
        {
            var models =
                (
                from p in _db.Products
                join c in _db.Categories on p.CategoryId equals c.Id
                where c.Name == name && p.Quantity > 0
                orderby p.Id descending
                select new ProductListViewModels
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                    Image = p.Image,
                    Detail = p.Detail,
                    Description = p.Description,
                    CategoryId = p.CategoryId,
                    CategoryName = c.Name
                }).ToPagedList(page, 12);

            ViewBag.name = name;
            ViewBag.max = models.PageCount;
            ViewBag.page = models.PageNumber;
            ViewBag.url = ControllerContext.RequestContext.HttpContext.Request;

            return View(models);
        }

        /// <summary>
        /// Return category list displayed on menu
        /// </summary
        /// <returns>A partial view embbeded in shared_layout</returns>
        public ActionResult GetCategoryList()
        {
            var model = from c in _db.Categories
                         select c;

            return PartialView("~/Views/Category/_CategoryListPartial.cshtml", model);
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}