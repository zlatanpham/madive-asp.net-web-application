﻿using MADIVE.ADL;
using MADIVE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MADIVE.Controllers
{
    public class HomeController : Controller
    {
        MadiveContext _db = new MadiveContext();

        // /index or /
        public ActionResult Index(int page = 1)
        {
            var models = 
                (
                from p in _db.Products
                join c in _db.Categories on p.CategoryId equals c.Id
                where p.Quantity > 0
                orderby p.Id descending 
                select new ProductListViewModels
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                    Image = p.Image,
                    Detail = p.Detail,
                    Description = p.Description,
                    CategoryId = p.CategoryId,
                    CategoryName = c.Name
                }).ToPagedList(page, 12);

            ViewBag.max = models.PageCount;
            ViewBag.page = models.PageNumber;
            ViewBag.url = ControllerContext.RequestContext.HttpContext.Request;

            return View(models);
        }

        protected override void Dispose(bool disposing)
        {
            if(_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}