﻿using MADIVE.ADL;
using MADIVE.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MADIVE.Controllers
{
    public class CartController : Controller
    {
        MadiveContext _db = new MadiveContext();
      
        // GET: Cart
        // /Cart
        public ActionResult Index(int page = 1)
        {
            var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>)Session["cart"];

            // Get sum price of all products
            ViewBag.totalPrice = cart.Sum(p => p.Quantity * p.Price);
            ViewBag.numOfItem = cart.Count();

            // Display just 4 items per page
            var models = cart.ToPagedList(page, 4);
            ViewBag.max = models.PageCount;
            ViewBag.page = models.PageNumber;

            return View(models);
        }

        /// <summary>
        /// Update the current items in cart when
        /// users update it
        /// </summary>
        /// <param name="id">the product id</param>
        /// <param name="quantity">the quantity of the product</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int id, int quantity)
        {
            var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>)Session["cart"];

            if (quantity == 0)
            {
                cart.RemoveAll(p => p.Id == id);
            }
            else
            {
                var updateItem = cart.Find(p => p.Id == id);
                updateItem.Quantity = quantity;
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        /// <summary>
        /// Display a quick preview of cart for last 5 items
        /// when users hover on the cart icon 
        /// </summary>
        /// <returns>Partial view with a list of 5 items in cart</returns>
        public ActionResult GetPreviewItemsInCart()
        {
            var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>) Session["cart"];

            ViewBag.numOfItem = cart.Count();
            ViewBag.totalPrice = 0;
            foreach (var item in cart)
            {
                ViewBag.totalPrice += item.Price * item.Quantity;
            }
            
            // Return the partial view along with just 5 last items in cart
            return PartialView("~/Views/Cart/_PreviewCart.cshtml", cart.Take(4).ToList());
        }

        /// <summary>
        /// Add an item or update the existing item in Cart
        /// </summary>
        /// <param name="Id">The Id of Product</param>
        /// <param name="quantity">The number of product to add</param>
        /// <returns>Json object contains the items in cart if the request is AJAX</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddToCart(int id, int quantity)
        {
            if (Session["cart"] == null)
            {
                Session["cart"] = new List<Product>();
            }

            var cart = (List<Product>)Session["cart"];
            var total = 0.0;

            var product = cart.Find(p => p.Id == id);
            // Update the quantity inside the session
            // in the case the product was in cart
            if (product != null)
            {
                if(quantity > 0)
                {
                    product.Quantity = quantity;
                }
                // Remove the item from cart if the quantity = 0
                else
                {
                    cart.RemoveAll(p => p.Id == id);
                    product = null;
                }               
            }
            // Otherwise, add a new product
            else
            {
                var refer = _db.Products.Find(id);
                product = new Product
                {
                    Id = id,
                    Name = refer.Name,
                    Price = refer.Price,
                    Image = refer.Image,
                    Quantity = quantity
                };
                cart.Insert(0, product);
            }

            if (!Request.IsAjaxRequest())
            {
                Response.Redirect(Request.UrlReferrer.ToString());
                return null;
            }
            else
            {
                // Get sum price of all products
                total = cart.Sum(p => p.Quantity * p.Price);
                // Fetch the last 5 items
                var last5items = cart.Take(4).ToList();
                
                var response = new
                {
                    id = id,
                    quantity = product == null ? 0 : product.Quantity,
                    total = total,
                    size = cart.Count(),
                    items = last5items
                };
                
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}