﻿using MADIVE.ADL;
using MADIVE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MADIVE.Controllers
{
    public class CheckoutController : Controller
    {
        MadiveContext _db = new MadiveContext();

        // GET: Checkout
        // /checkout
        [Authorize]
        public ActionResult Index()
        {
            var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>)Session["cart"];
            if(cart.Count() == 0)
            {
                return RedirectToAction("Index", "Cart");
            }
            else
            {   
                return View();
            }
        }

        // GET: Receipt
        // /checkout/receipt
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Receipt(CheckoutInfoModels info)
        {
            var cart = Session["cart"] == null
                ? new List<Product>()
                : (List<Product>)Session["cart"];

            if(ModelState.IsValid && cart.Count() > 0)
            {
                cart = (List<Product>)Session["cart"];
                ViewBag.totalPrice = cart.Sum(p => p.Price * p.Quantity);
                // Clone the all products in cart to send to view before clear it
                List<Product> clone = cart.ToList();
                // Remove all items in cart
                cart.Clear();

                return View(clone);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}