namespace MADIVE.Migrations
{
    using MADIVE.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MADIVE.ADL.MadiveContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MADIVE.ADL.MadiveContext context)
        {
            context.Categories.AddOrUpdate(c => c.Name,
                new Category { Name = "Book" },
                new Category { Name = "Decoration" },
                new Category { Name = "Bag" },
                new Category { Name = "Kitchen" }
            );

            Random random = new Random();

            for (int i = 0; i < 100; i++)
            {
                context.Products.AddOrUpdate(p => p.Name,
                    new Product
                    {
                        Name = "Product Number " + i,
                        Price = random.Next(20, 1000),
                        Quantity = 999,
                        Description = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
                        Detail = "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                        Image = "http://localhost:1954/img/product/" + random.Next(1, 40) + ".jpg",
                        CategoryId = random.Next(1, 5)
                    });
            };
        }
    }
}
