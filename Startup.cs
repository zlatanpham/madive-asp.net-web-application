﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MADIVE.Startup))]
namespace MADIVE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
