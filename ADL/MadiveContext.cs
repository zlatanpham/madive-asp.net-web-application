﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MADIVE.Models;

namespace MADIVE.ADL
{
    public class MadiveContext : DbContext
    {
        public MadiveContext() :base("name=MadiveContext")
        {
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}